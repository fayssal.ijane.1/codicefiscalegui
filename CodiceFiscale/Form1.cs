﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace CodiceFiscale
{
    public partial class Form1 : Form
    {
        
        public Form1()
        {
            dm = new DataManager();
            InitializeComponent();
            monthCalendar1.MaxDate = DateTime.Now;
            monthCalendar1.SetDate(monthCalendar1.MaxDate);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            dm.CodiceCatastaleDB();
            cmbLuoghi.Items.AddRange(dm.Mappa.Values.ToList().ToArray());
            Console.WriteLine("");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Nominativo nominativo = new Nominativo(txtNome.Text, txtCognome.Text);
            string nominativoFormattato = nominativo.getCognomeFormattato() + nominativo.getNomeFormattato();

            RadioButton rb = (rbMaschio.Checked) ? rbMaschio : rbFemmina;
            DataNascita dataNascita = new DataNascita(monthCalendar1.SelectionStart, rb);
            string dataFormattata = dataNascita.getDataNascita();

            string codiceCatastale = dm.Mappa.Single(x => x.Value.Equals(cmbLuoghi.SelectedItem.ToString())).Key;

            CarattereDiControllo cdc = new CarattereDiControllo(nominativoFormattato, dataFormattata, codiceCatastale);
            char carattereDiControllo = cdc.getCarattere();

            MessageBox.Show(nominativoFormattato + dataFormattata+ codiceCatastale + carattereDiControllo);
        }

        private void checkNominativo(object sender, EventArgs e)
        {
            TextBox t = (TextBox)sender;
            string nominativo = Regex.Replace(t.Text.ToString(), @"\W", "");
            string vocali = getVocali(nominativo);
            string consonanti = getConsonanti(nominativo);
            if ((consonanti.Length <= 2 && vocali.Length == 0) || consonanti.Length == 0)
            {
                if (t.Equals(txtCognome)) txtCognome.ForeColor = System.Drawing.Color.Red;
                else if (t.Equals(txtNome)) txtNome.ForeColor = System.Drawing.Color.Red;
            }
            else
            {
                if (t.Equals(txtCognome)) txtCognome.ForeColor = System.Drawing.Color.Black;
                else if (t.Equals(txtNome)) txtNome.ForeColor = System.Drawing.Color.Black;
            }
            if (txtCognome.ForeColor.Equals(System.Drawing.Color.Black) && txtNome.ForeColor.Equals(System.Drawing.Color.Black)) button1.Enabled = true;
            else button1.Enabled = false;
        }

        private String getConsonanti(String x)
        {
            return Regex.Replace(x.ToUpper(), @"[AEIOU]", "");
        }

        private String getVocali(String x)
        {
            return Regex.Replace(x.ToUpper(), @"[^AEIOU]", "");
        }
    }
}
