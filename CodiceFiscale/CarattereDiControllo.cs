﻿using System;

namespace CodiceFiscale
{
    internal class CarattereDiControllo
    {
        private string nominativoFormattato;
        private string dataFormattata;
        private string codiceCatastale;

        public CarattereDiControllo(string nominativoFormattato, string dataFormattata, string codiceCatastale)
        {
            this.nominativoFormattato = nominativoFormattato;
            this.dataFormattata = dataFormattata;
            this.codiceCatastale = codiceCatastale;
        }

        internal char getCarattere()
        {
            string codice = nominativoFormattato + dataFormattata + codiceCatastale;
            int temp;
            int sommaPari = 0, sommaDispari = 0, sommaTotale=0;
            int[] posizioneDispari = { 1, 0, 5, 7, 9, 13, 15, 17, 19, 21, 1, 0, 5, 7, 9, 13, 15, 17, 19, 21, 2, 4, 18, 20,
                11, 3, 6, 8, 12, 14, 16, 10, 22, 25, 24, 23 };
            char[] caratteriDiControllo = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F',
                'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
            for(int i=0; i< codice.Length; i++)
            {
                if (i % 2 != 0)
                {
                    try
                    {
                        temp =  int.Parse(codice[i].ToString());
                        sommaPari += temp;
                    }catch (FormatException)
                    {
                        sommaPari = sommaPari + (codice[i] - 'A');
                    }
                } else sommaDispari = sommaDispari + posizioneDispari[new string(caratteriDiControllo).IndexOf(codice[i])];
            }
            sommaTotale = sommaPari + sommaDispari;
            return (char)(sommaTotale % 26 + 'A');
        }
    }
}