﻿using System;

namespace CodiceFiscale
{
    internal class DataNascita
    {
        private DateTime selectionStart;
        private System.Windows.Forms.RadioButton rb;

        public DataNascita(DateTime selectionStart, System.Windows.Forms.RadioButton rb)
        {
            this.selectionStart = selectionStart;
            this.rb = rb;
        }

        public string getDataNascita()
        {
            string anno = selectionStart.ToString("yy");
            char[] mesiLettera = { 'A', 'B', 'C', 'D', 'E', 'H', 'L', 'M', 'P', 'R', 'S', 'T' };
            char mese = mesiLettera[selectionStart.Month - 1];
            string giorno = "";
            if(rb.Text.Equals("Maschio")) giorno=selectionStart.ToString("dd");
            else giorno =(selectionStart.Day+40).ToString();
            return anno+mese+giorno;
        }
    }
}