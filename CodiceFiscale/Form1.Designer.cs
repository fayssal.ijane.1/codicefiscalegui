﻿namespace CodiceFiscale
{
    partial class Form1
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblMain = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblCognome = new System.Windows.Forms.Label();
            this.lblNome = new System.Windows.Forms.Label();
            this.lblLuogo = new System.Windows.Forms.Label();
            this.lblSesso = new System.Windows.Forms.Label();
            this.lblData = new System.Windows.Forms.Label();
            this.txtCognome = new System.Windows.Forms.TextBox();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.monthCalendar1 = new System.Windows.Forms.MonthCalendar();
            this.cmbLuoghi = new System.Windows.Forms.ComboBox();
            this.rbMaschio = new System.Windows.Forms.RadioButton();
            this.rbFemmina = new System.Windows.Forms.RadioButton();
            this.button1 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(193)))), ((int)(((byte)(18)))));
            this.panel1.Controls.Add(this.lblMain);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Location = new System.Drawing.Point(19, 11);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(685, 126);
            this.panel1.TabIndex = 0;
            // 
            // lblMain
            // 
            this.lblMain.AutoSize = true;
            this.lblMain.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.lblMain.Font = new System.Drawing.Font("Arial Unicode MS", 19.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMain.ForeColor = System.Drawing.Color.White;
            this.lblMain.Location = new System.Drawing.Point(271, 65);
            this.lblMain.Name = "lblMain";
            this.lblMain.Size = new System.Drawing.Size(408, 44);
            this.lblMain.TabIndex = 1;
            this.lblMain.Text = "Calcolatore Codice Fiscale";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.InitialImage")));
            this.pictureBox1.Location = new System.Drawing.Point(1, 0);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(109, 126);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // lblCognome
            // 
            this.lblCognome.AutoSize = true;
            this.lblCognome.Font = new System.Drawing.Font("Arial Unicode MS", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCognome.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(0)))));
            this.lblCognome.Location = new System.Drawing.Point(12, 159);
            this.lblCognome.Name = "lblCognome";
            this.lblCognome.Size = new System.Drawing.Size(165, 38);
            this.lblCognome.TabIndex = 1;
            this.lblCognome.Text = "COGNOME";
            // 
            // lblNome
            // 
            this.lblNome.AutoSize = true;
            this.lblNome.Font = new System.Drawing.Font("Arial Unicode MS", 16.2F);
            this.lblNome.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(0)))));
            this.lblNome.Location = new System.Drawing.Point(12, 231);
            this.lblNome.Name = "lblNome";
            this.lblNome.Size = new System.Drawing.Size(101, 38);
            this.lblNome.TabIndex = 2;
            this.lblNome.Text = "NOME";
            // 
            // lblLuogo
            // 
            this.lblLuogo.AutoSize = true;
            this.lblLuogo.Font = new System.Drawing.Font("Arial Unicode MS", 16.2F);
            this.lblLuogo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(0)))));
            this.lblLuogo.Location = new System.Drawing.Point(12, 310);
            this.lblLuogo.Name = "lblLuogo";
            this.lblLuogo.Size = new System.Drawing.Size(285, 38);
            this.lblLuogo.TabIndex = 3;
            this.lblLuogo.Text = "LUOGO DI NASCITA";
            // 
            // lblSesso
            // 
            this.lblSesso.AutoSize = true;
            this.lblSesso.Font = new System.Drawing.Font("Arial Unicode MS", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSesso.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(0)))));
            this.lblSesso.Location = new System.Drawing.Point(12, 399);
            this.lblSesso.Name = "lblSesso";
            this.lblSesso.Size = new System.Drawing.Size(115, 38);
            this.lblSesso.TabIndex = 4;
            this.lblSesso.Text = "SESSO";
            // 
            // lblData
            // 
            this.lblData.AutoSize = true;
            this.lblData.Font = new System.Drawing.Font("Arial Unicode MS", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblData.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(0)))));
            this.lblData.Location = new System.Drawing.Point(397, 310);
            this.lblData.Name = "lblData";
            this.lblData.Size = new System.Drawing.Size(258, 38);
            this.lblData.TabIndex = 5;
            this.lblData.Text = "DATA DI NASCITA";
            // 
            // txtCognome
            // 
            this.txtCognome.BackColor = System.Drawing.Color.White;
            this.txtCognome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCognome.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCognome.Font = new System.Drawing.Font("Arial Unicode MS", 12F);
            this.txtCognome.Location = new System.Drawing.Point(19, 194);
            this.txtCognome.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtCognome.Name = "txtCognome";
            this.txtCognome.Size = new System.Drawing.Size(679, 34);
            this.txtCognome.TabIndex = 6;
            this.txtCognome.TextChanged += new System.EventHandler(this.checkNominativo);
            // 
            // txtNome
            // 
            this.txtNome.BackColor = System.Drawing.Color.White;
            this.txtNome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNome.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNome.Font = new System.Drawing.Font("Arial Unicode MS", 12F);
            this.txtNome.Location = new System.Drawing.Point(19, 273);
            this.txtNome.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(679, 34);
            this.txtNome.TabIndex = 7;
            this.txtNome.TextChanged += new System.EventHandler(this.checkNominativo);
            // 
            // monthCalendar1
            // 
            this.monthCalendar1.Location = new System.Drawing.Point(404, 357);
            this.monthCalendar1.Name = "monthCalendar1";
            this.monthCalendar1.TabIndex = 8;
            // 
            // cmbLuoghi
            // 
            this.cmbLuoghi.Font = new System.Drawing.Font("Arial Unicode MS", 12F);
            this.cmbLuoghi.FormattingEnabled = true;
            this.cmbLuoghi.Location = new System.Drawing.Point(19, 351);
            this.cmbLuoghi.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cmbLuoghi.Name = "cmbLuoghi";
            this.cmbLuoghi.Size = new System.Drawing.Size(373, 34);
            this.cmbLuoghi.TabIndex = 9;
            // 
            // rbMaschio
            // 
            this.rbMaschio.AutoSize = true;
            this.rbMaschio.Font = new System.Drawing.Font("Arial Unicode MS", 16.2F);
            this.rbMaschio.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(0)))));
            this.rbMaschio.Location = new System.Drawing.Point(133, 396);
            this.rbMaschio.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rbMaschio.Name = "rbMaschio";
            this.rbMaschio.Size = new System.Drawing.Size(143, 42);
            this.rbMaschio.TabIndex = 10;
            this.rbMaschio.TabStop = true;
            this.rbMaschio.Text = "Maschio";
            this.rbMaschio.UseVisualStyleBackColor = true;
            // 
            // rbFemmina
            // 
            this.rbFemmina.AutoSize = true;
            this.rbFemmina.Font = new System.Drawing.Font("Arial Unicode MS", 16.2F);
            this.rbFemmina.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(0)))));
            this.rbFemmina.Location = new System.Drawing.Point(133, 441);
            this.rbFemmina.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.rbFemmina.Name = "rbFemmina";
            this.rbFemmina.Size = new System.Drawing.Size(155, 42);
            this.rbFemmina.TabIndex = 11;
            this.rbFemmina.TabStop = true;
            this.rbFemmina.Text = "Femmina";
            this.rbFemmina.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(153)))), ((int)(((byte)(0)))));
            this.button1.Enabled = false;
            this.button1.Font = new System.Drawing.Font("Arial Unicode MS", 19.8F);
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(11, 487);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(380, 75);
            this.button1.TabIndex = 12;
            this.button1.Text = "CALCOLA";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(717, 573);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.rbFemmina);
            this.Controls.Add(this.rbMaschio);
            this.Controls.Add(this.cmbLuoghi);
            this.Controls.Add(this.monthCalendar1);
            this.Controls.Add(this.txtNome);
            this.Controls.Add(this.txtCognome);
            this.Controls.Add(this.lblData);
            this.Controls.Add(this.lblSesso);
            this.Controls.Add(this.lblLuogo);
            this.Controls.Add(this.lblNome);
            this.Controls.Add(this.lblCognome);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblMain;
        private System.Windows.Forms.Label lblCognome;
        private System.Windows.Forms.Label lblNome;
        private System.Windows.Forms.Label lblLuogo;
        private System.Windows.Forms.Label lblSesso;
        private System.Windows.Forms.Label lblData;
        private System.Windows.Forms.TextBox txtCognome;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.MonthCalendar monthCalendar1;
        private System.Windows.Forms.ComboBox cmbLuoghi;
        private System.Windows.Forms.RadioButton rbMaschio;
        private System.Windows.Forms.RadioButton rbFemmina;
        private System.Windows.Forms.Button button1;
        private DataManager dm;
    }
}

