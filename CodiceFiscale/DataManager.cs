﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Text.Json.Nodes;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace CodiceFiscale
{
    internal class DataManager
    {
        private MySqlConnection connection;
        private string server;
        private string database;
        private string username;
        private string password;
        private string connectionString;
        private Dictionary<string, string> _mappa;

        public DataManager()
        {
            this.server = "localhost";
            this.database = "codici_catastali";
            this.username = "root";
            this.password = "admin";
            this.connectionString = $"SERVER={server};DATABASE={database};UID={username};PASSWORD={password};";
        }

        public Dictionary<string, string> Mappa => _mappa;

        public void CodiceCatastaleDB()
        {
            try
            {
                if (_mappa != null) return;
                connection = new MySqlConnection(connectionString);
                connection.Open();
                MySqlCommand command = new MySqlCommand();
                command.Connection = connection;
                command.CommandText = "SELECT * FROM elenco";

                string s = "";
                MySqlDataReader reader = command.ExecuteReader();
                if (!reader.HasRows)
                {
                    throw new Exception("Nessun luogo trovato!");
                }
                else
                {
                    while (reader.Read())
                    {
                        _mappa.Add((string)reader["Denominazione"], (string)reader["codice_identificativo"]);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " Ricerca nel file JSON");
                CodiceCatastaleJSON();
            }
        }

        private void CodiceCatastaleJSON()
        {
            try
            {
                if (_mappa != null) return;
                IList<CodiceCatastale> asd = JsonArray.Parse(File.ReadAllText(string.Format("{0}/elenco.json", AppDomain.CurrentDomain.BaseDirectory))).AsArray().Deserialize<IList<CodiceCatastale>>();
                if (asd == null || asd.Count == 0)
                {
                    throw new Exception("Errore ricezione da file JSON");
                }
                _mappa = listToHashMap(asd);
                return;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "Ricerca nel file JSON");
                CodiceCatastaleXML();
            }
        }

        private void CodiceCatastaleXML()
        {
            try
            {
                if (_mappa != null) return;
                XmlSerializer s = new XmlSerializer(typeof(ListaCodiceCatastali));
                ListaCodiceCatastali lista = new ListaCodiceCatastali();
                using (FileStream reader = new FileStream(string.Format("{0}/elenco.xml", AppDomain.CurrentDomain.BaseDirectory), FileMode.Open))
                {
                    lista = (ListaCodiceCatastali)s.Deserialize(reader);
                }
                if (lista == null || lista.Lista == null || lista.Lista.Count <= 0)
                {
                    throw new Exception("Errore ricezione da file XML!");
                }
                _mappa = listToHashMap(lista.Lista);
                return;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "! Controllare se almeno una fonte dati è disponibile e riprovare accesso!");
            }
        }

        private Dictionary<string, string> listToHashMap(IList<CodiceCatastale> asd)
        {
            Dictionary<string, string> mappa = new Dictionary<string, string>();
            foreach (CodiceCatastale codice in asd)
            {
                mappa.Add(codice.CodiceIdentificativo, codice.Denominazione);
            }
            return mappa;
        }
    }
}
