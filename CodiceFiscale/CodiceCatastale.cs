﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace CodiceFiscale
{
    [XmlRoot(ElementName ="listaCodiciCatastali")]
    public class ListaCodiceCatastali
    {
        [XmlElement(ElementName ="row")]
        public List<CodiceCatastale> Lista { get; set; }
    }

    [XmlRoot(ElementName ="row")]
    public class CodiceCatastale
    {
        [JsonPropertyName("codiceIdentificativo")]
        [XmlElement(ElementName ="codiceIdentificativo")]
        public string CodiceIdentificativo { get; set;}

        [JsonPropertyName("denominazione")]
        [XmlElement(ElementName = "denominazione")]
        public string Denominazione { get; set;}
    }
}
