﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CodiceFiscale
{
    internal class Nominativo
    {
        private string nome;
        private string cognome;

        public Nominativo(string nome, string cognome){
            this.nome = nome;
            this.cognome = cognome;
        }
        public string getNomeFormattato()
        {
            nome = Regex.Replace(nome, @"\W", "");
            string vocali = getVocali(nome);
            string consonanti = getConsonanti(nome);
            if (consonanti.Length > 3)
            {
                nome = consonanti.Remove(1,1).Substring(0, 3);
            }
            else if (consonanti.Length == 2) nome = consonanti + vocali[0];
            else if (consonanti.Length == 1)
            {
                if (vocali.Length == 1) nome = consonanti + vocali[0] + 'X';
                else nome = consonanti + (vocali.Substring(0, 2));
            }
            else if (consonanti.Length == 3) nome = consonanti;

            return nome;
        }

        public string getCognomeFormattato()
        {
            cognome = Regex.Replace(cognome, @"\W", "");
            string vocali = getVocali(cognome);
            string consonanti = getConsonanti(cognome);
            if (consonanti.Length > 3) cognome = consonanti.Substring(0, 3);
            else if (consonanti.Length == 2) cognome = consonanti + vocali[0];
            else if (consonanti.Length == 1)
            {
                if (vocali.Length == 1) cognome = consonanti + vocali[0] + 'X';
                else cognome = consonanti + (vocali.Substring(0, 2));
            }
            else if (consonanti.Length == 3) cognome = consonanti;

            return cognome;
        }

        private string getConsonanti(string x)
        {
            return Regex.Replace(x.ToUpper(), @"[AEIOU]", "");
        }

        private string getVocali(string x)
        {
            return Regex.Replace(x.ToUpper(), @"[^AEIOU]", "");
        }
    }
}
